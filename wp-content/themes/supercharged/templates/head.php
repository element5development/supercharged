<!DOCTYPE HTML>
<html>
<head>
  <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php wp_title(''); ?></title>
    <?php wp_head(); ?>
    <link rel="icon" href="<?php echo site_url(); ?>/wp-content/themes/supercharged/public/img/favicon.ico" type="image/x-icon" />
    <link rel="shortcut icon" href="<?php echo site_url(); ?>/wp-content/themes/supercharged/public/img/favicon.ico" type="image/x-icon" />
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-141696804-1"></script>
    <script  type='text/javascript'>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', 'UA-141696804-1');

      /* Track Waiver goals via onClick */
      var captureClickGoal = function(category, action, label) {
    
        ga('create', 'UA-141696804-1', 'auto');

        ga('send', {
          hitType: 'event',
          eventCategory: category,
          eventAction: action,
          eventLabel: label
        });
       
      } 
    </script>
    <!-- Global site tag (gtag.js) - Google Ads: 726806305 --> 
    <script async src="https://www.googletagmanager.com/gtag/js?id=AW-726806305"></script> 
    <script> 
    window.dataLayer = window.dataLayer || []; function gtag(){dataLayer.push(arguments);} gtag('js', new Date()); gtag('config', 'AW-726806305'); 

    /* Track AdWords Goal via onClick */
    var captureAdWordsGoal = function(goalID) {

      gtag('event', 'conversion', { 'send_to': 'AW-726806305/'+goalID });

    }
    </script>
    <?php 

    //echo $post->ID;

    if($post->ID == '5') { // home page

      $pixel_ca = '20820946';

    } elseif($post->ID == '843') { // buy gift cards page

      $pixel_ca = '20820947';    
    
    } elseif($post->ID == '19') { // activities page

      $pixel_ca = '20823305';    
    
    } elseif($post->ID == '879') { // view - kids birthday page

      $pixel_ca = '20823304';
    
    } elseif($post->ID == '927') { // view - adults birthday page

      $pixel_ca = '20823307';
        
    } elseif($post->ID == '942') { // view - bachelor/ette

      $pixel_ca = '20823490';
        
    } elseif($post->ID == '657') { // book - kids

      $pixel_ca = '20823306';
        
    } elseif($post->ID == '1373') { // book - corporate/private

      $pixel_ca = '20823308';
        
    } elseif($post->ID == '1378') { // book - bachelor/ette

      $pixel_ca = '20823491';
        
    } else { // all other pages  

      $pixel_ca = '20820948';

    }
    ?>
    <script type='text/javascript'>
    (function() {
     var w = window, d = document;
     var s = d.createElement('script');
     s.setAttribute('async', 'true');
     s.setAttribute('type', 'text/javascript');
     s.setAttribute('src', '//c1.rfihub.net/js/tc.min.js');
     var f = d.getElementsByTagName('script')[0];
     f.parentNode.insertBefore(s, f);
     if (typeof w['_rfi'] !== 'function') {
      w['_rfi']=function() {
       w['_rfi'].commands = w['_rfi'].commands || [];
       w['_rfi'].commands.push(arguments);
      };
     }
     _rfi('setArgs', 'ver', '9');
     _rfi('setArgs', 'rb', '25867');
     _rfi('setArgs', 'ca', '<?php echo $pixel_ca; ?>');
     _rfi('setArgs', '_o', '25867');
     _rfi('setArgs', '_t', '<?php echo $pixel_ca; ?>');
     _rfi('track');
    })();
    </script>
    <noscript>
      <iframe src='//20820948p.rfihub.com/ca.html?rb=25867&ca=<?php echo $pixel_ca; ?>&_o=25867&_t=<?php echo $pixel_ca; ?>&ra={{Random Number}}' style='display:none;padding:0;margin:0' width='0' height='0'>
    </iframe>
    </noscript>
    <?php if($_GET['type'] == 'adult' || $_GET['type'] == 'kids') { ?>
    <!-- START: Track "Book Now" clicks for Adults and Kids parties -->	
    <?php $book_pixel_ca = $_GET['type'] == 'adult' ? '20823308' : '20823306'; ?>
    <script type='text/javascript'>
    (function() {
     var w = window, d = document;
     var s = d.createElement('script');
     s.setAttribute('async', 'true');
     s.setAttribute('type', 'text/javascript');
     s.setAttribute('src', '//c1.rfihub.net/js/tc.min.js');
     var f = d.getElementsByTagName('script')[0];
     f.parentNode.insertBefore(s, f);
     if (typeof w['_rfi'] !== 'function') {
      w['_rfi']=function() {
       w['_rfi'].commands = w['_rfi'].commands || [];
       w['_rfi'].commands.push(arguments);
      };
     }
     _rfi('setArgs', 'ver', '9');
     _rfi('setArgs', 'rb', '25867');
     _rfi('setArgs', 'ca', '<?php echo $book_pixel_ca; ?>');
     _rfi('setArgs', '_o', '25867');
     _rfi('setArgs', '_t', '<?php echo $book_pixel_ca; ?>');
     _rfi('track');
    })();
    </script>
    <noscript>
    <iframe src='//20823308p.rfihub.com/ca.html?rb=25867&ca=<?php echo $book_pixel_ca; ?>&_o=25867&_t=<?php echo $book_pixel_ca; ?>&ra={{Random Number}}' style='display:none;padding:0;margin:0' width='0' height='0'>
    </iframe>
    </noscript>
    <!-- END: Track "Book Now" clicks for Adults and Kids parties -->
    <?php } ?>
</head>
