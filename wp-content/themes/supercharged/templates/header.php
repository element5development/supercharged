<body>

<!-- DEFAULT HEADER -->
<div class="header">
    <div class="container">
        <div class="row">
            <div class="col-lg-4 col-md-10">
                <div class="header_logo">
                <?php $logo = wp_get_attachment_image_src(get_field('header_logo','option'), 'large');?>
                    <a href="<?php echo site_url(); ?>"><img src="<?php  echo $logo[0]; ?>"></a>
                </div>
            </div>
            <div class="col-lg-8 col-md-2">
                <div class="header_mobile"><i class="fa fa-bars"></i></div>
                <div class="header_right">
                    <?php  wp_nav_menu(array('menu_class'=>'header_nav','menu_id'=>'menu-main-menu','theme_location'=>'primary_nav','container' => false ));?>
                    <div class="header_button">
                        
                        <div class="account-navigation">

                            <?php
                                echo do_shortcode('[re-supercharged-login-status
                                    container-class="account-navigation__inline"
                                    profile-class="account-navigation__profile"
                                    :show-profile-in-profile="true"
                                    :no-sign-in-link="true"
                                    :no-sign-out-link="true"
                                ]')
                            ?>

                            <a href="/checkout" class="account-navigation__card"><span id="shoppingCartItemCount" class="account-navigation__count" style="display: none;"></span></a>
                            <script>
                                jQuery(function() {
                                    window._ = typeof _ !== 'undefined' ? _ : {};
                                    window._.get =  _.get || function (obj, path, def) {
                                        if (typeof obj !== 'object') {
                                            return undefined;
                                        }
                                        var value = obj, paths = (''+path).split('.') || [];
                                        for(var key in paths) {
                                            value = value[paths[key]];
                                            if (value === undefined) {
                                                return def;
                                            }
                                        }
                                        return value;
                                    };
                                    window._.reject = _.reject || function (obj, callback) {
                                        if (typeof obj !== 'object') {
                                            return undefined;
                                        }
                                        var newObj = {};
                                        for(var key in obj) {
                                            if (!callback(obj[key], key, obj)) {
                                                newObj[key] = obj[key];
                                            }
                                        }
                                        return newObj;
                                    };

                                    var ITEMSPATH = 'Shop/Orders';
                                    var counter = jQuery('#shoppingCartItemCount');

                                    function cartCount() {
                                        var storage = localStorage.getItem(ITEMSPATH);
                                        if (storage) {
                                            storage = JSON.parse(storage);
                                            var items = window._.get(storage, ITEMSPATH + '.Cart.items', [])
                                            items = window._.reject(items, function(item) {
                                                if (item.attributes.skipCartCount) {
                                                    return item.attributes.skipCartCount;
                                                }
                                            });
                                            var count = Object.keys(items).length
                                            if (count > 0) {
                                                counter.text(count);
                                                counter.show();
                                            } else {
                                                counter.hide();
                                            }
                                        }
                                    }

                                    if (counter.length) {
                                        cartCount();

                                        var EventBus = window.VueEventBus
                                        if (EventBus) {
                                            EventBus.$on('cart-item-deleted', function() {
                                                cartCount();
                                            });
                                        }
                                    }

                                });
                            </script>

                            <?php
                                echo do_shortcode('[re-supercharged-login-status
                                    container-class="account-navigation__inline"
                                    sign-in-class="account-navigation__sign"
                                    sign-out-class="account-navigation__signout"
                                    :no-profile-link="true"
                                    :sign-out-only-in-profile="false"
                                ]')
                            ?>
                        </div>

                        <?php if(get_field('gifts_card_link')){ ?>
                          <a href="<?php  echo get_field('gifts_card_link', wp_get_nav_menu_object( 2 ));?>" onclick="captureClickGoal('GiftCard', 'Click', 'GiftCard'); captureAdWordsGoal('<?php the_sub_field('button_goal_id_gift_card'); ?>');" class="site_button"><?php  echo get_field('gifts_card', wp_get_nav_menu_object( 2 ));?></a>
                        <?php } else { ?>
                          <a href="<?php  echo get_field('gift_card_external_link', wp_get_nav_menu_object( 2 ));?>" onclick="captureClickGoal('GiftCard', 'Click', 'GiftCard'); captureAdWordsGoal('<?php the_sub_field('button_goal_id_gift_card'); ?>');" class="site_button" target="_blank"><?php  echo get_field('gifts_card', wp_get_nav_menu_object( 2 ));?></a>
                        <?php } ?>
                        <a href="<?php  echo get_field('contact_link', wp_get_nav_menu_object( 2 ));?>" onclick="captureAdWordsGoal('<?php the_sub_field('button_goal_id_contact'); ?>');" class="site_button"><?php  echo get_field('contact_text', wp_get_nav_menu_object( 2 ));?></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

