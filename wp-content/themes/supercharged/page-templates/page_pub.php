<?php
/*
Template Name: Pub
*/
?>
<?php get_header(); ?>

<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

<?php  $src = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), array( 1905,350 ), false, '' );?>

<div class="title_bar title_bar_large" style="background-image:url(<?php echo $src[0];?>);">
	<div class="container">
		<h1><?php the_title();?></h1>
	</div>
</div>

<div class="se_main_content">
	<div class="container">

	         <?php if( have_rows('hero_section') ): ?>
                      <?php while ( have_rows('hero_section') ) : the_row();  ?>

		<h2><?php the_sub_field('title'); ?></h2>
		<p><?php the_sub_field('content'); ?></p>
		<br/><br/>
		<h2><?php the_sub_field('hours_title'); ?></h2>
		<p><?php the_sub_field('hours'); ?></p>

		      <?php  endwhile; ?>  
                       <?php endif; ?>
	</div>
         
             <?php if( have_rows('view_menu') ): ?>
                   <?php while ( have_rows('view_menu') ) : the_row();  ?>
                    <?php $image = wp_get_attachment_image_src(get_sub_field('image'), 'large');?>

                    <div class="se_inner_content " >
                         <?php if( have_rows('images') ): ?>
                                    <div class="row">
                                      <?php while ( have_rows('images') ) : the_row();  ?>
                                        <?php $image = wp_get_attachment_image_src(get_sub_field('image'), 'large');?>       
                                            <div class="col-sm-2 pub_grub_images" >
                                                <div class="pub_grub_image" style="background-image: linear-gradient(rgba(0, 0, 0, 0.1), rgba(0, 0, 0, 0.1)), url(<?php  echo $image[0]; ?>);"></div>
                                            </div>
                                        <?php  endwhile; ?>  
                                    </div>
                                <?php endif; ?>
                        <div class="container">
                            <?php if(get_sub_field('button_link')){ ?>
                                <a href="<?php the_sub_field('button_link'); ?>" class="site_button"><?php the_sub_field('button_text'); ?></a>
                            <?php } ?>
                        </div>
                    </div>

	       <?php  endwhile; ?>  
                    <?php endif; ?>


          <?php if( have_rows('beers_on_tap') ): ?>
                  <?php while ( have_rows('beers_on_tap') ) : the_row();  ?>
                    <?php $background = wp_get_attachment_image_src(get_sub_field('background_image'), 'large');?>          

                    <div class="se_inner_content" style="background-image:url(<?php  echo $background[0]; ?>);">
                        <div class="container">
                            <h2><?php the_sub_field('table_title'); ?></h2>
                            <ul class="se_beer_list">

                                        <?php if( have_rows('menu') ): ?>
                                             <?php while ( have_rows('menu') ) : the_row();  ?>

                                    <li>
                                        <h5><?php the_sub_field('name'); ?></h5>
                                        <p><?php the_sub_field('style'); ?><br><?php the_sub_field('available_location'); ?></p>
                                    </li>

                                         <?php  endwhile; ?>  
                                                 <?php endif; ?>	


                            </ul>
                        </div>
                    <?php if(get_field('beers_bottom_cotent')){ ?>
                        <?php the_field('beers_bottom_cotent'); ?>
                    <?php } ?>
                    </div>

	          <?php  endwhile; ?>  
            <?php endif; ?>


	<div class="container">

	    <?php if( have_rows('daily_sections') ): ?>
                  <?php while ( have_rows('daily_sections') ) : the_row();  ?>

		<h2><?php the_sub_field('daily_title'); ?></h2>
		<div class="se_deals_slider">

           <?php if( have_rows('daily_deals') ): ?>
              <?php while ( have_rows('daily_deals') ) : the_row();  ?>
                <?php $image = wp_get_attachment_image_src(get_sub_field('background_image'), 'large');?>

                <div class="se_deals_slide">
                    <div class="se_deals_slide_content" style="background-image:url(<?php if(get_sub_field('background_image')){ echo $image[0]; } else{ echo site_url().'/wp-content/themes/supercharged/public/img/taps.jpg';   }?>);">
                        <div class="se_deals_slide_content_inner">
                            <h3><?php the_sub_field('title'); ?></h3>
                            <h5><?php the_sub_field('sub_title'); ?></h5>
                            <p><?php the_sub_field('content'); ?></p>
                        </div>
                    </div>
                </div>

             <?php  endwhile; ?>  
            <?php endif; ?>

		</div>

		   <?php  endwhile; ?>  
        <?php endif; ?>

        <br/><br/><br/><br/>

         <?php if( have_rows('calendar_section') ): ?>
           <?php while ( have_rows('calendar_section') ) : the_row();  ?>

                <h2><?php the_sub_field('title'); ?></h2>
                <p><?php the_sub_field('content'); ?></p>
                <?php if(get_sub_field('button_link')){ ?>
                    <div class="se_content_button">
                        <a href="<?php the_sub_field('button_link'); ?>" class="site_button site_button_inverse"><?php the_sub_field('button_text'); ?></a>
                    </div>
                <?php } ?>
                <?php  endwhile; ?>  
            <?php endif; ?>

	</div>
</div>

   <?php $background = wp_get_attachment_image_src(get_field('form_background'), 'large');?>

<div class="se_main_content_background se_main_content_background_reverse" style="background-image:url(<?php  echo $background[0]; ?>);">
	<div class="container">
		<h2><?php the_field('question_titile'); ?></h2>
		<?php  the_content();?>
		<div class="se_contact_form">

			<?php  echo do_shortcode('[gravityform id="5" title="false" description="false" ajax="true"]');?>
			
		</div>

          

	</div>
</div>

<?php endwhile; endif; ?>

<?php get_footer(); ?>