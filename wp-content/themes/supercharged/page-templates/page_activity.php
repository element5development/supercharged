<?php
/*
Template Name: Activity
*/
?>
<?php get_header(); ?>

<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

<?php  $src = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), array( 1905,250 ), false, '' );?>	

<div class="title_bar title_bar_large" style="background-image:url(<?php echo $src[0];?>);">
	<div class="container">
		<h1><?php the_title();?></h1>
	</div>
</div>

<div class="se_interior_nav">
	<div class="container">
		<h3>Jump to:</h3>
		<ul>
            <?php if(get_field('pricing_title_a')){ ?>
                <li><a href="#pricing"><?php the_field('pricing_title_a'); ?></a></li>
            <?php } ?>
			    <?php while ( have_rows('waiver_section_a') ) : the_row();  ?>
			<li><a href="#waiver"><?php the_sub_field('title'); ?></a></li>
			    <?php  endwhile; ?> 

			   <?php while ( have_rows('booking_section') ) : the_row();  ?> 
			<li><a href="#prebooking"><?php the_sub_field('title'); ?></a></li>
			   <?php  endwhile; ?> 

		</ul>
	</div>
</div>


<div class="se_main_content">
	<div class="se_photo_slider">
        <?php if( have_rows('photo_slider') ): ?>
          <?php while ( have_rows('photo_slider') ) : the_row();  ?>
            <?php $image = wp_get_attachment_image_src(get_sub_field('image'), 'large');?>

            <div class="se_photo_slide">
                <a href="<?php  echo $image[0]; ?>" data-caption="<?php the_sub_field('caption_text'); ?>">
                    <div class="se_photo_slide_image <?php if(get_sub_field('check_if_vertical_image')){ ?> vertical <?php } ?>">
                        <img src="<?php  echo $image[0]; ?>">
                    </div>
                </a>
            </div> 	

          <?php  endwhile; ?>  
        <?php endif; ?>
	</div>
</div>


<?php if( have_rows('first_section') ): ?>
       <?php while ( have_rows('first_section') ) : the_row();  ?>
            <div class="se_main_content_blue">
                <div class="container">
                    <h3><?php the_sub_field('title'); ?></h3>
                    <p><?php the_sub_field('content'); ?></p>
                </div>
            </div>
    <?php  endwhile; ?>  
<?php endif; ?>


<?php if( have_rows('second_section') ): ?>
    <?php while ( have_rows('second_section') ) : the_row();  ?>     
        <?php $bgImage = get_sub_field('background_image'); ?>

            <div class="se_main_content_background se_main_content_background_reverse" style="background-image:url(<?php echo $bgImage['url']; ?>;">
                <div class="container">
                    <h3><?php the_sub_field('title'); ?></h3>
                    <p><?php the_sub_field('content'); ?></p>
                    <div class="se_content_button">

                        <a class="site_button" data-toggle="modal" data-target="#myModal"><?php the_sub_field('button_text'); ?></a>
                    </div>
                </div>
            </div>
  <!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog modal-lg">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body">
            <video muted loop controls>
            <source src="<?php the_sub_field('video_file'); ?>" type="video/mp4">   
            </video>
        </div>
      </div>
      
    </div>
  </div>
    <?php  endwhile; ?>  
<?php endif; ?>





<div class="se_main_content">
	<div class="container">
        <?php if( have_rows('pricing_section_a')) { ?>
		<div id="pricing">
			<h2><?php the_field('pricing_title_a'); ?></h2>

           <?php if( have_rows('pricing_section_a') ): ?>
             <?php while ( have_rows('pricing_section_a') ) : the_row();  ?>

                <p><?php the_sub_field('pricing _content'); ?></p>
                <div class="se_pricing_boxes">
                    <div class="row">
                    <?php 
                    $row_count = count(get_sub_field('pricing_table')); 
                    if( have_rows('pricing_table') ):
                             while ( have_rows('pricing_table') ) : the_row();  ?>

                        <div class="col-md-12 <?php if($row_count == 2){ ?> col-lg-6<?php } elseif($row_count == 1) { ?>col-lg-12<?php } else { ?> col-lg-4 <?php } ?> se_pricing_box_wrap">
                            <div class="se_pricing_box">
                                <h4><?php the_sub_field('table_title'); ?></h4>
                                <table class="se_pricing_table">
                                    <?php if(!(get_sub_field('first_column_title') && get_sub_field('second_column_title') && get_sub_field('third_column_title'))) { ?>
                                    <tr>
                                        <th><?php the_sub_field('first_column_title'); ?></th>
                                        <th><?php the_sub_field('second_column_title'); ?></th>
                                        <?php if(get_sub_field('third_column_title')){ ?>
                                            <th class="third_title"><?php the_sub_field('third_column_title'); ?></th>
                                        <?php } ?>
                                    </tr>
                                    <?php } ?>

                                          <?php if( have_rows('table_row') ): ?>
                                            <?php while ( have_rows('table_row') ) : the_row();  ?>

                                    <tr>
                                        <td><?php the_sub_field('title'); ?></td>
                                        <td><?php the_sub_field('price'); ?></td>
                                        <?php if(get_sub_field('pre_price')){ ?>
                                           <td><?php the_sub_field('pre_price'); ?></td>
                                        <?php } ?>

                                    </tr>	
                                        <?php  endwhile; ?>  
                                    <?php endif; ?>

                                </table>
                            </div>
                        </div>

                        <?php  endwhile; ?>  
                    <?php endif; ?>

				</div>
			</div>

               <?php  endwhile; ?>  
            <?php endif; ?>


		</div>
        <?php } ?> 
            <!-- Safety Section -->
            <?php if( have_rows('safety_section') ): ?>
                <?php while ( have_rows('safety_section') ) : the_row();  ?>
                <!-- if no safety rule list, dont show -->
                 <?php if( get_sub_field('safety_rule_list') ){ ?>
                    <div class="se_top_divider">Back to Top <i class="fas fa-arrow-alt-circle-up"></i></div>

                    <div id="safety">
                            <h2><?php the_sub_field('title'); ?></h2>
                            <p><?php the_sub_field('content'); ?></p>

                        <?php if( have_rows('safety_rule_list') ): ?>
                        <ol>
                            <?php while ( have_rows('safety_rule_list') ) : the_row();  ?>
                            <li><?php the_sub_field('rule'); ?></li>
                            <?php  endwhile; ?>  
                        </ol>
                        <?php endif; ?>

                    </div>
        
                    <?php } // ends get field safety rule list?>
                <?php  endwhile; ?>  
            <?php endif; ?>
        
        <div class="se_top_divider">Back to Top <i class="fas fa-arrow-alt-circle-up"></i></div>

        <?php if( have_rows('waiver_section_a') ): ?>
             <?php while ( have_rows('waiver_section_a') ) : the_row();  ?>

            <div id="waiver">
                <h2><?php the_sub_field('title'); ?></h2>
                <p><?php the_sub_field('content'); ?></p>
                
                <?php 
                //Button link
                $link = get_sub_field('button_link');
                $link_url = $link['url'];
                $link_target = $link['target'] ? $link['target'] : '_self';
                ?>

                <div class="se_content_button">
                    <a href="<?php echo esc_url($link_url); ?>" onclick="captureClickGoal('Waiver', 'Click', 'Waiver');" class="site_button site_button_inverse" target="<?php echo esc_attr($link_target); ?>"><?php the_sub_field('button_text'); ?></a>
                </div>
            </div>

            <div class="se_top_divider">Back to Top <i class="fas fa-arrow-alt-circle-up"></i></div>  

           <?php  endwhile; ?>  
        <?php endif; ?>
        <!-- More Section A -->
         <?php if( have_rows('more_section_a') ): ?>
             <?php while ( have_rows('more_section_a') ) : the_row();  ?>

                <div id="#child-party">
                    <h2><?php the_sub_field('title'); ?></h2>
                    <?php the_sub_field('content'); ?>
                    <?php 
                    //Button link
                    $link = get_sub_field('button_link');
                    $link_url = $link['url'];
                    $link_target = $link['target'] ? $link['target'] : '_self';
                    ?>
                    <div class="se_content_button">
                            <a href="<?php echo esc_url($link_url); ?>" <?php if($post->post_parent == 19){ ?>onclick="captureClickGoal('PreBook', 'Click', 'PreBook'); captureAdWordsGoal('<?php the_sub_field('button_goal_id'); ?>');" <?php } else { ?>onclick="captureClickGoal('BookPartyEvent', 'Click', 'BookPartyEvent'); captureAdWordsGoal('<?php the_sub_field('button_goal_id'); ?>');" <?php } ?> class="site_button" target="<?php echo esc_attr($link_target); ?>"><?php the_sub_field('button_text'); ?></a>
                    </div>
                </div>

                <div class="se_top_divider">Back to Top <i class="fas fa-arrow-alt-circle-up"></i></div>

            <?php  endwhile; ?>  
        <?php endif; ?>

	</div>
</div>



   <?php if( have_rows('booking_section') ): ?>
         <?php while ( have_rows('booking_section') ) : the_row();  ?>
            <?php $bgImage = get_sub_field('background_image'); ?>

        <div class="se_main_content_background se_main_content_background_reverse" style="background-image:url(<?php echo $bgImage['url']; ?>;">
            <div class="container">
                <div id="prebooking">
                    <h2><?php the_sub_field('title'); ?></h2>
                    <p><?php the_sub_field('content'); ?></p>
                    <div class="se_content_button">
                        <?php if(get_sub_field('button_link')){ ?>
                            <!-- if it has parent page Events, do BookPartyEvent for SEM tag, else is if it is on activity page, do PreBook SEM tag -->
                            <a href="<?php the_sub_field('button_link'); ?><?php if(get_sub_field('tracking_id')){ the_sub_field('tracking_id'); } ?>" <?php if($post->post_parent == 19){ ?>onclick="captureClickGoal('PreBook', 'Click', 'PreBook'); captureAdWordsGoal('<?php the_sub_field('button_goal_id'); ?>');" <?php } else { ?>onclick="captureClickGoal('BookPartyEvent', 'Click', 'BookPartyEvent');" <?php } ?> class="site_button site_button_inverse"><?php the_sub_field('button_text'); ?></a>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>

   <?php  endwhile; ?>  
<?php endif; ?>



<?php endwhile; endif; ?>

<?php get_footer(); ?>
