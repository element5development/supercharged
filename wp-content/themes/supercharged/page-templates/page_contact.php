<?php
/*
Template Name: Contact
*/
?>
<?php get_header(); ?>

<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

<?php  $src = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), array( 1905,250 ), false, '' );?>	

<div class="title_bar" style="background-image:url(<?php echo $src[0];?>);">
	<div class="container">
		<h1><?php the_title();?></h1>
	</div>
</div>

<div class="se_main_content se_main_content_full">
	<div class="container">
		<div class="se_body_content">
			<h3><?php the_field('sub_title'); ?></h3>
			<?php  the_content();?>
		</div>

		<div class="se_contact_content">
			<div class="row">
				<div class="col-lg-6 order-lg-2">

					<?php  echo do_shortcode('[gravityform id="1" title="false" description="false" ajax="true"]');?>
					
				</div>
				<div class="col-lg-6 order-lg-1">
					<div class="se_info_map">
                        <iframe width="600" height="500" id="gmap_canvas" src="https://maps.google.com/maps?q=591%20Washington%20St%20%20Wrentham%2C%20MA%2002093&t=&z=13&ie=UTF8&iwloc=&output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe>
					</div>
				</div>
			</div>
		</div>

	</div>
</div>

       <?php if( have_rows('last_section') ): ?>
                      <?php while ( have_rows('last_section') ) : the_row();  ?>
                      	<?php $background = wp_get_attachment_image_src(get_sub_field('background_image'), 'large');?>  

<div class="se_main_content_background" style="background-image:url(<?php  echo $background[0]; ?>);">
	<div class="container">
		<h2><?php the_sub_field('title'); ?></h2>
		<p><?php the_sub_field('content'); ?></p>
		<div class="se_content_button">
			<a href="<?php the_sub_field('button_link'); ?>" onclick="captureClickGoal('Contact', 'Click', 'Contact');" class="site_button" target="_blank"><?php the_sub_field('button_text'); ?></a>
		</div>
	</div>
</div>
          <?php  endwhile; ?>  
                   <?php endif; ?>
								



<?php endwhile; endif; ?>

<?php get_footer(); ?>