<?php
/*
Template Name: Default Rocket Effect Template
Author: Rocket Effect
*/
?>

<?php get_header(); ?>

<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

<?php  $src = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), array( 1905,250 ), false, '' );?>	

<div class="title_bar" style="background-image:url(<?php echo $src[0];?>);">
  <div class="container">
    <h1><?php the_title();?></h1>
  </div>
</div>

<div class="page-general re-sticky-wrapper">
  <div class="page-container">
    <div class="page-body-content page-general-body-content">
      <?php  the_content();?>
    </div>
  </div>
  <div class="re-sticky-push"></div>
</div>

</div>

<?php endwhile; endif; ?>

<?php get_footer(); ?>
