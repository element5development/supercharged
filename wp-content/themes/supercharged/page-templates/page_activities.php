<?php
/*
Template Name: Activities
*/
?>
<?php get_header(); ?>

<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

	<?php  $src = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), array( 1905,250 ), false, '' );?>	

<div class="title_bar title_bar_large" style="background-image:url(<?php echo $src[0];?>);">
	<div class="container">
		<h1><?php the_title();?></h1>
	</div>
</div>

<div class="se_main_content se_main_content_full">
	<div class="container">
		<?php  the_content();?>

		<div class="se_attraction_blocks">

		            <?php if( have_rows('activities') ): ?>
                      <?php while ( have_rows('activities') ) : the_row();  ?>
                      	<?php $image = wp_get_attachment_image_src(get_sub_field('image'), 'large');
                      	 $icon = wp_get_attachment_image_src(get_sub_field('icon'), 'large');?> 

             <div class="se_attraction_block">
				<a href="<?php echo get_sub_field('link'); ?>">
					<div class="se_attraction_block_image">
						<img src="<?php  echo $image[0]; ?>">
					</div>
					<div class="se_attraction_block_content">
						<div class="se_attraction_icon">
							<img src="<?php  echo $icon[0]; ?>">
						</div>
						<h3><?php the_sub_field('title'); ?></h3>
					</div>
				</a>
			</div>   	

					  <?php  endwhile; ?>  
                       <?php endif; ?>


		</div>
	</div>
</div>


<?php endwhile; endif; ?>

<?php get_footer(); ?>