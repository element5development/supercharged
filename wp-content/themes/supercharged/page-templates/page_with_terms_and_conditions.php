<?php
/*
Template Name: With Terms & Conditions
Author: Rocket Effect
*/
?>

<?php get_header(); ?>

<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

<?php  $src = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), array( 1905,250 ), false, '' );?>	

<div class="title_bar" style="background-image:url(<?php echo $src[0];?>);">
  <div class="container">
    <h1><?php the_title();?></h1>
  </div>
</div>

<div class="page-general re-sticky-wrapper">
  <div class="page-container">
    <div class="page-body-content page-general-body-content">
        <div class="list-conditions">
            <p class="list-conditions__title"><?php echo get_field('title'); ?></p>
            <h3 class="list-conditions__caption"><?php echo get_field('caption'); ?></h3>
            <?php echo get_field('list'); ?>
            <h3 class="list-conditions__caption"><?php echo(!empty(get_field('title_#2')))?get_field('title_#2'):''; ?></h3>
            <?php echo(!empty(get_field('list_#2')))?get_field('list_#2'):''; ?>
        </div>
        <?php  the_content();?>
    </div>
  </div>
  <div class="re-sticky-push"></div>
</div>

<?php endwhile; endif; ?>

<?php get_footer(); ?>
