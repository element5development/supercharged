<?php
/*
Template Name: Events
*/
?>
<?php get_header(); ?>

<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

<?php  $src = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), array( 1905,250 ), false, '' );?>	

<div class="title_bar" style="background-image:url(<?php echo $src[0];?>);">
	<div class="container">
		<h1><?php the_title();?></h1>
	</div>
</div>

<div class="se_interior_nav">
	<div class="container">
		<ul>
          <?php if( have_rows('events') ): $a=1;?>
              <?php while ( have_rows('events') ) : the_row();  ?>
                 <li><a href="#event<?php echo $a;?>"><?php the_sub_field('menu_title'); ?></a></li>    	
                <?php  $a++; endwhile; ?>  
            <?php endif; ?>
		</ul>
	</div>
</div>

<div class="se_main_content se_main_content_full se_main_content_events">

                 <?php $Background = wp_get_attachment_image_src(get_field('event_background'), 'large');?> 
                 
	<div class="se_inner_content" style="background-image:url(<?php  echo $Background[0]; ?>);">
		<div class="container">
            <?php if( have_rows('events') ): $a=1;?>
                <?php while ( have_rows('events') ) : the_row(); ?>
                    <?php $image = wp_get_attachment_image_src(get_sub_field('image'), 'large');?> 

                    <div class="se_event_section" id="event<?php echo $a;?>">
                        <div class="row">
                            <div class="col-lg-5  <?php if($a%2==0){ echo 'order-lg-2';}?>">
                                <div class="se_event_photo">
                                    <img src="<?php  echo $image[0]; ?>">
                                </div>
                            </div>
                            <div class="col-lg-7 <?php if($a%2==0){ echo 'order-lg-1';}?>  ">
                                <div class="se_event_text">
                                    <div class="se_event_text_inner">
                                        <h3><?php the_sub_field('title'); ?></h3>
                                        <p><?php the_sub_field('content'); ?></p>
                                        <?php if(get_sub_field('button_email_link')){ ?>
                                            <div class="se_content_button">
                                                <a href="mailto:<?php the_sub_field('button_email_link'); ?>" class="site_button  <?php if($a%2!=0){ echo 'site_button_inverse';}?>  " onclick="captureClickGoal('BookPartyEvent', 'Click', 'BookPartyEvent'); captureAdWordsGoal('<?php the_sub_field('button_goal_id'); ?>');"><?php the_sub_field('button_text'); ?></a>
                                            </div>	
                                        <?php } else { ?>
                                            <div class="se_content_button">
                                                <a href="<?php the_sub_field('button_link'); ?><?php if(get_sub_field('tracking_id')){ the_sub_field('tracking_id'); } ?>" class="site_button  <?php if($a%2!=0){ echo 'site_button_inverse';}?>  " onclick="captureClickGoal('BookPartyEvent', 'Click', 'BookPartyEvent'); captureAdWordsGoal('<?php the_sub_field('button_goal_id'); ?>');"><?php the_sub_field('button_text'); ?></a>
                                            </div>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>       	

              <?php  $a++; endwhile; ?>  
            <?php endif; ?>
		</div>
	</div>
</div>

<?php endwhile; endif; ?>

<?php get_footer(); ?>
