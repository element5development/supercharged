<?php
/*
Template Name: Info
*/
?>
<?php get_header(); ?>

<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
	
<?php  $src = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), array( 1905,250 ), false, '' );?>	

<div class="title_bar" style="background-image:url(<?php echo $src[0];?>);">
	<div class="container">
		<h1><?php the_title();?></h1>
	</div>
</div>

<div class="se_interior_nav">
	<div class="container">
		<ul>
			<li><a href="#pricing"><?php the_field('pricing_title'); ?></a></li>
			<li><a href="#hours"><?php the_field('location_title'); ?> </a></li>
			<li><a href="#faq"><?php the_field('faq_title'); ?></a></li>
			<li><a href="#gallery"><?php the_field('photo_gallery_title'); ?></a></li>
			<li><a href="#waiver"><?php the_field('waiver_title'); ?></a></li>
			<li><a href="#booking"><?php the_field('pre_booking_title'); ?></a></li>
		</ul>
	</div>
</div>

             

<div class="se_main_content se_main_content_full">
	<div class="container">

                 

		<div class="se_info_section" id="pricing">
			<h2><?php the_field('pricing_title'); ?></h2>

			     <?php if( have_rows('pricing_section') ): ?>
                      <?php while ( have_rows('pricing_section') ) : the_row();  ?>

			<p><?php the_sub_field('price_content'); ?></p>
			<div class="se_pricing_boxes">
				<div class="row">

				         <?php if( have_rows('pricing') ): ?>
                        <?php while ( have_rows('pricing') ) : the_row();  ?>

					<div class="col-lg-4 col-md-12">
						<div class="se_pricing_box">
							<h4><?php the_sub_field('table_title'); ?></h4>

							<table class="se_pricing_table">
                            <?php if(!(get_sub_field('first_column_title') && get_sub_field('second_column_title') && get_sub_field('third_column_title'))) { ?>
                                <tr>
                                    <th><?php the_sub_field('first_column_title'); ?></th>
                                    <th><?php the_sub_field('second_column_title'); ?></th>
                                    <?php if(get_sub_field('third_column_title')){ ?>
                                        <th class="third_title"><?php the_sub_field('third_column_title'); ?></th>
                                    <?php } ?>
                                </tr>
                            <?php } ?>
							       <?php if( have_rows('table_row') ): ?>
                                        <?php while ( have_rows('table_row') ) : the_row();  ?>
								<tr>
									<td><?php the_sub_field('title'); ?></td>
                                    
									   <td><?php the_sub_field('price'); ?></td>
                                    
                                    <?php if(get_sub_field('per_price')){ ?>
									   <td><?php the_sub_field('per_price'); ?></td>
                                    <?php } ?>
								</tr>

								   <?php  endwhile; ?>  
                                     <?php endif; ?>
								
							</table>
						</div>
					</div>

					   <?php  endwhile; ?>  
                          <?php endif; ?>

				</div>
			</div>

                  <?php  endwhile; ?>  
                     <?php endif; ?>

		</div>

		    

		<div class="se_top_divider">Back to Top <i class="fas fa-arrow-alt-circle-up"></i></div>

		<div class="se_info_section" id="hours">
			<h2> <?php the_field('location_title'); ?>  </h2>
			<div class="se_info_map">
                        <iframe width="600" height="500" id="gmap_canvas" src="https://maps.google.com/maps?q=591%20Washington%20St%20%20Wrentham%2C%20MA%2002093&t=&z=13&ie=UTF8&iwloc=&output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe>
			</div>

			    <?php if( have_rows('location_section') ): ?>
                      <?php while ( have_rows('location_section') ) : the_row();  ?>

			<div class="se_info_text">
				<h3><?php the_sub_field('location_title'); ?></h3>
				<p><?php the_sub_field('address'); ?></p>
				<p><a href="<?php the_sub_field('direction_link'); ?>" target="_blank"><?php the_sub_field('direction_text'); ?></a></p>
				<br/><br/>
				<h3><?php the_sub_field('hours_title'); ?></h3>

				     <?php if( have_rows('hours') ): ?>
                       <?php while ( have_rows('hours') ) : the_row();  ?>

				<p><?php the_sub_field('hour'); ?></p>

				   	 <?php  endwhile; ?>  
                        <?php endif; ?>

				
			</div>

			      <?php  endwhile; ?>  
                     <?php endif; ?>
		</div>

		<div class="se_top_divider">Back to Top <i class="fas fa-arrow-alt-circle-up"></i></div>

		<div class="se_info_section" id="faq">
			<h2> <?php the_field('faq_title'); ?> </h2>
			<div class="se_faq">

			         <?php if( have_rows('faq') ): ?>
                       <?php while ( have_rows('faq') ) : the_row();  ?>

				<div class="se_faq_section">
					<div class="se_faq_q">
						<h5><?php the_sub_field('question'); ?></h5>
						<i class="fas fa-plus"></i>
					</div>
					<div class="se_faq_a">
						<p><?php the_sub_field('answer'); ?></p>
					</div>
				</div>

				    <?php  endwhile; ?>  
                        <?php endif; ?>

			</div>
            <div class="se_content_button">
				<a href="<?php the_field('faq_button_link'); ?>" class="site_button"><?php the_field('faq_button'); ?></a>
			</div>
		</div>

		<div class="se_top_divider">Back to Top <i class="fas fa-arrow-alt-circle-up"></i></div>

		<div class="se_info_section" id="gallery">
			<h2><?php the_field('photo_gallery_title'); ?></h2>
			<div class="se_info_gallery_slider">

			        <?php if( have_rows('photo_gallery') ): ?>
                       <?php while ( have_rows('photo_gallery') ) : the_row();  ?>
                       	<?php $image = wp_get_attachment_image_src(get_sub_field('image'), 'large');?>

				<div class="se_info_gallery_slide">
					<div class="se_info_gallery_slide_image"><img src="<?php  echo $image[0]; ?>"></div>
				</div>

				   <?php  endwhile; ?>  
                        <?php endif; ?>

			</div>
			<div class="se_info_gallery_slider_thumbs">

			    <?php if( have_rows('photo_gallery') ): ?>
                       <?php while ( have_rows('photo_gallery') ) : the_row();  ?>
                       	<?php $image = wp_get_attachment_image_src(get_sub_field('image'), 'large');?>

				<div class="se_info_gallery_slide">
					<div class="se_info_gallery_slide_image"><img src="<?php  echo $image[0]; ?>"></div>
				</div>

				    <?php  endwhile; ?>  
                        <?php endif; ?>

			</div>
		</div>

		<div class="se_top_divider">Back to Top <i class="fas fa-arrow-alt-circle-up"></i></div>


           

		<div class="se_info_section" id="waiver">
			<h2><?php the_field('waiver_title'); ?></h2>

              <?php if( have_rows('waiver_section') ): ?>
                 <?php while ( have_rows('waiver_section') ) : the_row();  ?>

                    <p><?php the_sub_field('content'); ?></p>
                    <?php 
                    //Button link
                    $link = get_sub_field('button_link');
                    $link_url = $link['url'];
                    $link_target = $link['target'] ? $link['target'] : '_self';
                    ?>
            
                    <div class="se_content_button">
                    <a href="<?php echo esc_url($link_url); ?>" onclick="captureClickGoal('Waiver', 'Click', 'Waiver');" class="site_button site_button_inverse" target="<?php echo esc_attr($link_target); ?>"><?php the_sub_field('button_text'); ?></a>

                    </div>

			  <?php  endwhile; ?>  
            <?php endif; ?>

		</div>

		    

		<div class="se_top_divider">Back to Top <i class="fas fa-arrow-alt-circle-up"></i></div>

		<div class="se_info_section" id="booking">
			<h2><?php the_field('pre_booking_title'); ?></h2>

			   <?php if( have_rows('booking_section') ): ?>
                   <?php while ( have_rows('booking_section') ) : the_row();  ?>

                    <p><?php the_sub_field('content'); ?></p>
                    
                    <div class="se_content_button">
                        <?php if(get_sub_field('button_link')){ ?>
                            <a href="<?php the_sub_field('button_link'); ?>" onclick="captureClickGoal('PreBook', 'Click', 'PreBook'); captureAdWordsGoal('<?php the_sub_field('button_goal_id'); ?>');" class="site_button site_button_inverse"><?php the_sub_field('button_text'); ?></a>
                        <?php } else { ?>
                            <a class="site_button site_button_inverse" onclick="captureClickGoal('PreBook', 'Click', 'PreBook'); captureAdWordsGoal('<?php the_sub_field('button_goal_id'); ?>');"><?php the_sub_field('button_text'); ?></a>
                        <?php } ?>
                    </div>

			   <?php  endwhile; ?>  
            <?php endif; ?>

		</div>

		<div class="se_top_divider">Back to Top <i class="fas fa-arrow-alt-circle-up"></i></div>

	</div>
</div>

<div class="se_main_content_background" style="background-image:url(<?php echo site_url(); ?>/wp-content/themes/supercharged/public/img/arcade.jpg);">
	<div class="container">
		<h2><?php the_field('get_in_touch_title'); ?></h2>

            <?php if( have_rows('touch_section') ): ?>
                   <?php while ( have_rows('touch_section') ) : the_row();  ?>

		<p><?php the_sub_field('content'); ?></p>
		<div class="se_content_button">
			<a href="<?php the_sub_field('button_link'); ?>" onclick="captureClickGoal('Contact', 'Click', 'Contact'); captureAdWordsGoal('<?php the_sub_field('button_goal_id'); ?>');" class="site_button"><?php the_sub_field('button_text'); ?></a>
		</div>

		     <?php  endwhile; ?>  
                   <?php endif; ?>

	</div>
</div>


<?php endwhile; endif; ?>

<?php get_footer(); ?>
