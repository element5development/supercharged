var $ = jQuery; 
$(document).ready(function(){

	$('.hero_slider').slick({
		arrows:false,
		autoplay:true,
		speed:2000,
		autoplaySpeed:4000,
		dots:false,
		fade:true
	});

	// smooth scrolling
	$(function() {
	  $('a[href*=#]:not([data-toggle="tab"]):not([data-toggle="pill"])').click(function() {
	    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
	      var target = $(this.hash);
	      target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
	      if (target.length) {
	        $('html,body').animate({
	          scrollTop: target.offset().top - 100
	        }, 500);
	        return false;
	      }
	    }
	  });
	});

	$(window).scroll(function(){
		if($(window).scrollTop() > 0){
			$('.header').addClass('header_scrolled');
		} else {
			$('.header').removeClass('header_scrolled');
		}
	});

	$('body').scroll(function(){
		if($('body').scrollTop() > 0){
			$('.header').addClass('header_scrolled');
		} else {
			$('.header').removeClass('header_scrolled');
		}
	});

	// check if is touch device
	function isTouchDevice() {
	    return 'ontouchstart' in document.documentElement;
	}

	if (isTouchDevice()){
		$( '.header_nav li.menu-item-has-children a:not(.sub-menu li a)' ).on('click', function(e) {
		    if( !$(this).is('.clicked') ) {
		        e.preventDefault();
		        $(this).addClass('clicked');
		    }
		});
	}

	$('.header_mobile').click(function(){
		$('.header_right').slideToggle();
		$(this).find('i').toggleClass('fa-bars').toggleClass('fa-times');
	});
	
	$('.se_photo_slider').slick({
		infinite:true,
		slidesToShow:4,
		slidesToScroll:1,
		centerMode:true,
		arrows:true,
		responsive: [
		{
		  breakpoint: 1199,
		  settings: {
		    slidesToShow: 4
		  }
		},
		{
		  breakpoint: 991,
		  settings: {
		    slidesToShow: 3
		  }
		},
		{
		  breakpoint: 768,
		  settings: {
		    slidesToShow: 2
		  }
		},
		{
		  breakpoint: 576,
		  settings: {
		    slidesToShow: 1
		  }
		}
		]
	});

	$('.se_photo_slider').slickLightbox({
		itemSelector: 'a',
		navigateByKeyboard: true,
		caption: 'caption'
	});

	$('.se_top_divider').click(function(){
		$('html, body').animate({
		    scrollTop: 0
		 }, 1000);
	});

	$('.se_deals_slider').slick({
		slidesToShow:3,
		slidesToScroll:1,
		arrows:true,
		responsive: [
		{
		  breakpoint: 991,
		  settings: {
		    slidesToShow: 2
		  }
		},
		{
		  breakpoint: 768,
		  settings: {
		    slidesToShow: 1
		  }
		}
		]
	});

	$('.se_faq_q').click(function(){
		$('.se_faq_q i').removeClass('fa-minus').addClass('fa-plus');
		$('.se_faq_section .se_faq_a').hide();
		$(this).find('i').removeClass('fa-plus').addClass('fa-minus');
		$(this).closest('.se_faq_section').find('.se_faq_a').toggle();
	});

	$('.se_info_gallery_slider').slick({
		slidesToShow:1,
		asNavFor:'.se_info_gallery_slider_thumbs',
		arrows:false,
		fade:true
	});

	$('.se_info_gallery_slider_thumbs').slick({
		slidesToShow:7,
		asNavFor:'.se_info_gallery_slider',
		arrows:true,
		focusOnSelect: true,
		responsive: [
		{
		  breakpoint: 1199,
		  settings: {
		    slidesToShow: 6
		  }
		},
		{
		  breakpoint: 991,
		  settings: {
		    slidesToShow: 5
		  }
		},
		{
		  breakpoint: 767,
		  settings: {
		    slidesToShow: 3
		  }
		},
		{
		  breakpoint: 575,
		  settings: {
		    slidesToShow: 2
		  }
		}
		]
	});

});