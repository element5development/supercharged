<?php get_header(); ?>

<div class="title_bar" style="background-image:url(<?php echo site_url(); ?>/wp-content/themes/supercharged/public/img/arcade.jpg);">
  <div class="container">
    <h1><h1> 404 PAGE </h1></h1>
  </div>
</div>

<div class="se_main_content se_main_content_full">
  <div class="container">
    <div class="se_body_content">
      <div class="row">
      <div class="col-lg-10 offset-lg-1">
           <h1  style="text-align:center;color:white"  >Page Not Found</h1>
               <p style=" text-align:center;"><?php _e( "Sorry, we can't find the page you're looking for.", 'HTML5 Reset' ); ?></p>
          <p class="error_404" style=" text-align:center;">Please return <a href="<?php echo site_url();?>">home</a> and try again.</p>
      </div>
    </div>
    </div>

  </div>
</div>

  
<?php get_footer(); ?>




