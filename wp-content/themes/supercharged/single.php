<?php get_header(); ?>

<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

<?php 
$blog_title = get_the_title( get_option('page_for_posts', true) );
$blog_img = get_the_post_thumbnail_url( get_option('page_for_posts', true) );
?>


<div class="title_bar" style="background-image:url(<?php echo $blog_img; ?>);">
    <div class="container">
        <p class="single_post"><?php echo $blog_title; ?></p>
    </div>
</div>

<div class="se_main_content se_main_content_full se_post_content">
	<div class="container">
		<div class="se_body_content">
			<h1 class="blog_post_title"><?php the_title(); ?></h1>
			<h5><?php the_date(); ?></h5>
			<hr>
			<div class="se_post_content_image">
				<img src="<?php echo get_the_post_thumbnail_url(); ?>">
			</div>	
            <?php the_content();?>
		</div>
	</div>
</div>


<?php endwhile; endif; ?>

<?php get_footer(); ?>