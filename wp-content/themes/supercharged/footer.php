<div class="footer">
	<div class="container">
		<div class="row">
			<div class="col-md-3 col-sm-6 order-1">
				<h5><?php  echo get_field('footer_menu_title', wp_get_nav_menu_object( 3 ));?></h5>
				<?php  wp_nav_menu(array('menu_class'=>'footer_links','theme_location'=>'footer_nav','container' => false ));?>
			</div>
			<div class="col-md-6 col-sm-12 order-md-2 order-3">
				<ul class="footer_social">

					 <?php
                        if( have_rows('social_network', 'options') ):
                        while ( have_rows('social_network', 'options') ) : the_row(); ?>

                  <li><a href="<?php the_sub_field('link'); ?>" onclick="captureClickGoal('Social', 'Click', 'Social');" target="_blank"><i class="fab <?php the_sub_field('font_class'); ?>"></i></a></li>  

                       <?php endwhile;  endif; ?>
				</ul>
				<div class="footer_logo">
				    <?php $logo = wp_get_attachment_image_src(get_field('footer_logo','option'), 'large');?>
					<img src="<?php  echo $logo[0]; ?>">
				</div>
				<div class="footer_form">
					<h5> <?php the_field('form_title', 'options'); ?>  </h5>

					<?php  echo do_shortcode('[gravityform id="2" title="false" description="false" ajax="true"]');?>
				</div>
			</div>
			<div class="col-md-3 col-sm-6 order-md-3 order-2">
				<div class="footer_info">

				      <?php if( have_rows('location_section', 'options') ):
                        while ( have_rows('location_section', 'options') ) : the_row(); ?>

					<h5><?php the_sub_field('location_title'); ?></h5>
					<ul class="footer_list">  

					    <?php if( have_rows('location') ):
                        while ( have_rows('location') ) : the_row(); ?>

                        <li><?php the_sub_field('text'); ?></li>

                        <?php endwhile;  endif; ?>

						<li><a href="<?php the_sub_field('direction_link'); ?>" target="_blank"><?php the_sub_field('direction_text'); ?></a></li>
					</ul>

					    <?php endwhile;  endif; ?>


					     <?php if( have_rows('hours_section', 'options') ):
                        while ( have_rows('hours_section', 'options') ) : the_row(); ?>

					<h5><?php the_sub_field('title'); ?></h5>
					<ul class="footer_list">

					  <?php if( have_rows('hours') ):
                        while ( have_rows('hours') ) : the_row(); ?>

						<li><?php the_sub_field('time'); ?></li>
					
						     <?php endwhile;  endif; ?>
					</ul>

					       <?php endwhile;  endif; ?>
				</div>
			</div>
		</div>
	</div>
	<div class="footer_bottom">
		<div class="container">
			<p>Supercharged Entertainment &#183; Copyright &copy; <?php echo date('Y'); ?> Supercharged Entertainemnt. All Rights Reserved. &#183; <a href="https://www.yellingmule.com/services" target="_blank">Yelling Mule</a> - <a href="https://www.yellingmule.com" target="_blank">Boston Web Design</a> -<br>Ecommerce and Attraction Booking System by <a href="https://rocketeffect.com" target="_blank">Rocket Effect</a></p>
		</div>
	</div>
</div>

<?php wp_footer(); ?>
</body>
</html>

