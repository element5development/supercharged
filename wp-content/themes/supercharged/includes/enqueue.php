<?php

/**

* Theme Styles

*/

function yellingmule_register_styles(){

  wp_register_style(

    'font-google',

    'https://fonts.googleapis.com/css?family=Open+Sans:300,400,700|Russo+One',

     null, //no dependencies

    null

  );

  wp_register_style(

    'font-awesome',

    'https://use.fontawesome.com/releases/v5.0.6/css/all.css',

     null, //no dependencies

    null

  );

    wp_register_style(

    'bootstrap',

    'https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css',

     null, //no dependencies

    null

  );


     wp_register_style(

    'slick',

    get_stylesheet_directory_uri().'/public/js/slick/slick.css',

     null, //no dependencies

    null

  );
    
   wp_register_style(

    'slick-theme',

    get_stylesheet_directory_uri().'/public/js/slick/slick-theme.css',

     null, //no dependencies

    null

  );
   
    wp_register_style(

    'lightbox-style',

    'https://cdnjs.cloudflare.com/ajax/libs/slick-lightbox/0.2.12/slick-lightbox.css',

     null, //no dependencies

    null

  );

   wp_register_style(

    'main-style', //handle

     get_stylesheet_directory_uri().'/style.css?v=1.34', //source

    null, //no dependencies

    null //version

  ); 

   wp_register_style(

    'mobile-media',

    get_stylesheet_directory_uri().'/public/css/media.css',

     null, //no dependencies

    null

  );



}

add_action('init', 'yellingmule_register_styles');  

  function yellingmule_enqueue_styles(){
  
  wp_enqueue_style('font-google'); //Font-google
  wp_enqueue_style('font-awesome'); //Font-awesome
  wp_enqueue_style('bootstrap'); //Bootstrap.css
  wp_enqueue_style('slick'); //Slick.css
  wp_enqueue_style('slick-theme'); //Slick-theme.css
  wp_enqueue_style('lightbox-style'); //lightbox-style.css
  wp_enqueue_style('main-style'); //Main-style.css
  wp_enqueue_style('mobile-media'); //Mobile-media.css
  

  }

  add_action('wp_enqueue_scripts', 'yellingmule_enqueue_styles', 100);



// This template has all enqueue scripts and Styles

/**

* Theme Scripts

*/

function yellingmule_register_scripts() {



  wp_register_script(

  'slick-js', 

    get_stylesheet_directory_uri().'/public/js/slick/slick.min.js',

    array('jquery'),

    null,

    true

  );

  wp_register_script(

  'lightbox-js', 

   'https://cdnjs.cloudflare.com/ajax/libs/slick-lightbox/0.2.12/slick-lightbox.js',

    array('jquery'),

    null,

    true

  );

  wp_register_script(

  'bootstrap-js', 

   'https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js',

    array('jquery'),

    null,

    true

  );

    wp_register_script(

    'main-js', 

    get_stylesheet_directory_uri().'/public/js/jquery.sitewide.js',

    array('jquery'),

    null,

    true

  );



}

add_action('init', 'yellingmule_register_scripts');

function yellingmule_enqueue_scripts(){

    wp_enqueue_script('slick-js'); //Slick.js
    wp_enqueue_script('lightbox-js'); //Lightbox.js
    wp_enqueue_script('bootstrap-js'); //Bootstrap.js
    wp_enqueue_script('main-js'); //Main.js
 
}

add_action('wp_enqueue_scripts', 'yellingmule_enqueue_scripts');

?>