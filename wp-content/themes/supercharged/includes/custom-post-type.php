<?php 

//Team Custom post type
function team_register_post_type() {
	
	$singular = 'Member';
	$plural = 'Team';
	$text_domain = 'bizchecks';
	$slug = str_replace( ' ', '_', strtolower( $singular ) );
	$labels = array(
	

         'name'  => _x( $plural , 'Post Type General Name', $text_domain ),
		'singular_name'         => _x( $singular, 'Post Type Singular Name', $text_domain ),
		'menu_name'             => __( $plural, $text_domain ),
		'name_admin_bar'        => __( $plural, $text_domain ),
		'archives'              => __( $singular .'Archives', $text_domain ),
		'parent_item_colon'     => __( 'Parent '.$singular.':', $text_domain ),
		'all_items'             => __( 'All '.$plural, $text_domain ),
		'add_new_item'          => __( 'Add '.$singular, $text_domain),
		'add_new'               => __( 'Add '.$singular, $text_domain),
		'new_item'              => __( 'New '.$singular, $text_domain ),
		'edit_item'             => __( 'Edit '.$singular, $text_domain ),
		'update_item'           => __( 'Update '.$singular, $text_domain ),
		'view_item'             => __( 'View '.$singular, $text_domain ),
		'search_items'          => __( 'Search'. $plural, $text_domain ),
		'not_found'             => __( 'No '.$plural.' found', $text_domain ),
		'not_found_in_trash'    => __( 'No '.$plural.' found in Trash', $text_domain ),
		'featured_image'        => __( 'Featured Image', $text_domain ),
		'set_featured_image'    => __( 'Set featured image', $text_domain ),
		'remove_featured_image' => __( 'Remove featured image', $text_domain ),
		'use_featured_image'    => __( 'Use as featured image', $text_domain ),
		'insert_into_item'      => __( 'Insert into'.$singular, $text_domain),
		'uploaded_to_this_item' => __( 'Uploaded to this'.$singular, $text_domain ),
		'items_list'            => __( $plural .' list', $text_domain ),
		'items_list_navigation' => __( $plural .' list navigation', $text_domain ),
		'filter_items_list'     => __( 'Filter'. $plural. 'list', $text_domain ),

		);
	$args = array(
		

       'label'                => __( $singular, $text_domain),
		'description'           => __( 'Product custom post type description', $text_domain ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'author', 'thumbnail', 'page-attributes'  ),
		'hierarchical'          => true,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'menu_icon'           => 'dashicons-groups',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,		
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
		 // 'rewrite'       	=> array( 'slug' =>'garden-event' ),


	);
	register_post_type( "team", $args );
}
add_action( 'init', 'team_register_post_type' );