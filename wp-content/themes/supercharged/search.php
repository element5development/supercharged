<?php get_header(); ?>

<?php if (have_posts()) : while (have_posts()) : the_post(); ?>


<div class="title_bar">
    <div class="container">
        <h1 class="search-title"><?php echo $wp_query->found_posts; ?> Search Results Found For: "<?php the_search_query(); ?>"</h1>
    </div>
</div>

<div class="bcp_content">
 <div class="container">
        <div class="row">

    <?php if (have_posts()) : ?>
        <?php while (have_posts()) : the_post(); ?>

            <?php $link_url = substr(get_the_content(), 0, 4) == 'http' ? get_the_content() : get_the_permalink(); ?>
            <?php $url_target = substr(get_the_content(), 0, 4) == 'http' ? ' target="_blank"' : ''; ?>

            <div class="col-md-4 col-sm-6">
                <div class="ml_block">
                    <a href="<?php echo $link_url; ?>"<?php echo $url_target; ?>>
                        <div class="ml_block_photo">
                        
<?php global $post;?>
<?php $src = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), array( 5600,1000 ), false, '' );?>

                            <img src="<?php if(get_field('post_thumbnail')){ the_field('post_thumbnail');}
                            elseif($src[0]) { echo $src[0]; }
                            else { echo site_url().'/wp-content/uploads/2017/10/flag-heart-min.png';}  ?>">

                        </div>
                        <div class="ml_block_text">
                            <h3><?php the_title(); ?></h3>
                            <?php the_excerpt();?>
                            <p>Read More <i class="fa fa-angle-right"></i></p>
                        </div>
                    </a>
                </div>
            </div>

        <?php endwhile; ?>

        </div>
                               <!--   Paginate start -->

                        <div  <?php if ( is_paginated() ) : ?> class="post_pagination"<?php endif; ?> >

                             <div class="col-sm-4 right new_post "> <?php previous_posts_link('<< Newer Posts ') ;  ?> </div>  

                              <div class="col-sm-4 page_pagination">  
                                      <?php $current_page = max( 1, get_query_var('paged') );
                                    $total_pages = $wp_query->max_num_pages;
                                    echo 'Page '.$current_page.' of '.$total_pages;  ;  ?>  
                               </div>  

                              <div class="col-sm-4 old_post "> <?php next_posts_link('Older Posts >>'); ?>  </div>

          
                           </div>

                           <!--   Paginate start -->


    <?php else : ?>

        <h3 style="text-align:center;">No Results</h3>
           <p style=" text-align:center;margin-bottom:50px;">Sorry, we can't find anything for your search terms.</p>

    <?php endif; ?>
        
    </div>
</div>


<?php endwhile; endif; ?>

<?php get_footer(); ?>

