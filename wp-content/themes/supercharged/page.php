
<?php get_header(); ?>

<!-- is thank you page -->
<?php if(is_page(657)){ ?> 
    <script>
        captureClickGoal('Contact', 'Click', 'Contact');
    </script>
<?php }?>

<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

<?php  $src = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), array( 1905,250 ), false, '' );?>	

<div class="title_bar" style="background-image:url(<?php echo $src[0];?>);">
	<div class="container">
		<h1><?php the_title();?></h1>
	</div>
</div>

<div class="se_main_content se_main_content_full">
  <div class="container">
    <div class="se_body_content">
        <?php  the_content();?>
        
        <!-- FAQS -->
        <?php if(is_page(539)){ ?>
        <div class="se_info_section" id="faq">
			<h2> <?php the_field('faq_title'); ?> </h2>
			<div class="se_faq">

			         <?php if( have_rows('faqs') ): ?>
                       <?php while ( have_rows('faqs') ) : the_row();  ?>

				<div class="se_faq_section">
					<div class="se_faq_q">
						<h5><?php the_sub_field('title'); ?></h5>
						<i class="fas fa-plus"></i>
					</div>
					<div class="se_faq_a">
						<p><?php the_sub_field('content'); ?></p>
					</div>
				</div>

				    <?php  endwhile; ?>  
                        <?php endif; ?>

			</div>
		</div>
        <?php } ?>
        
    </div>

  </div>
</div>


<?php endwhile; endif; ?>

<?php get_footer(); ?>





