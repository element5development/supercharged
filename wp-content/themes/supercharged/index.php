<?php get_header(); ?>

<?php 
$blog_title = get_the_title( get_option('page_for_posts', true) );
$blog_img = get_the_post_thumbnail_url( get_option('page_for_posts', true) );
?>

<div class="title_bar" style="background-image:url(<?php echo $blog_img; ?>);">
    <div class="container">
        <h1><?php echo $blog_title; ?></h1>
    </div>
</div>

<div class="se_main_content se_main_content_full">
    <div class="container">
        <div class="se_blog_posts">
            <div class="row">
               <?php if (have_posts()): while (have_posts()) : the_post(); ?> 
                  <div class="col-lg-4 col-md-6">
                     <div class="se_post">
                        <a href="<?php the_permalink();?>">
                           <div class="se_post_image" style="background-image:url(<?php echo get_the_post_thumbnail_url(); ?>);">
                              <div class="se_post_image_hover">
                                 <h5>Read More</h5>
                              </div>
                           </div>
                           <div class="se_post_info">
                              <h5><?php the_title();?></h5>
                              <p class="date"><?php the_time('F j, Y');?></p>
                           </div>
                        </a>
                     </div>
                  </div>
              <?php endwhile; endif; ?>
            </div>

            <div <?php if ( is_paginated() ): ?> class="post_pagination"<?php endif; ?> >
               <div class="col-sm-4 right new_post">
                  <?php previous_posts_link('<< Newer Posts ') ;  ?>
               </div>  
               <div class="col-sm-4 page_pagination">  
                  <?php $current_page = max( 1, get_query_var('paged') );
                  $total_pages = $wp_query->max_num_pages;
                  echo 'Page '.$current_page.' of '.$total_pages;  ;  ?>  
               </div>  
               <div class="col-sm-4 old_post">
                  <?php next_posts_link('Older Posts >>'); ?>
               </div>
            </div>
        </div>
    </div>
</div>

<?php get_footer(); ?>
