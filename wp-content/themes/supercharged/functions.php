<?php
//require_once get_template_directory()   . '/includes/custom-post-type.php';        // Custom Post Type Registration
require_once get_stylesheet_directory().'/includes/enqueue.php';        // Scripts & Styles enqueue


 //Translation can be available in /lang/ directory *

load_theme_textdomain( 'aia', get_template_directory() . '/lang' );

$locale = get_locale();
$locale_file = get_template_directory() . "/lang/$locale.php";
if ( is_readable( $locale_file ) )
  require_once( $locale_file );

// Change WordPress Logo on Admin
function my_login_logo() {
?>
<style type="text/css">

body.login {
    background:linear-gradient(to bottom, rgba(0,84,135,1) 1%,rgba(0,0,0,1) 100%) !important;
}
.login.wp-core-ui .button-primary {
    background:#005487 !important;
    border-color:rgba(0,84,135,1) !important;
    box-shadow:0 1px 0 rgba(0,84,135,1) !important;
    text-shadow:none !important;
}
.login.wp-core-ui .button-primary:hover {
    background:#000 !important;
    border-color:#000 !important;
    box-shadow:0 1px 0 #000 !important;
    text-shadow:none !important;
    color: #fff !important;
}
.login form {
    margin-top: 20px;
    margin-left: 0;
    padding: 26px 24px 46px;
    background: #fff;
    box-shadow: 0 1px 3px #000 !important;
}
.login input[type=text]:focus, .login input[type=password]:focus, .login input[type=checkbox]:focus, .login input[type=color]:focus, .login input[type=email]:focus {
    border-color: #000;
    box-shadow: 0 0 2px #000;
}
.login input[type=checkbox]:checked:before {
    content: "\f147";
    margin: -3px 0 0 -4px;
    color: rgba(0,84,135,1);
}
.login label {
    color: black !important;
}
.login #backtoblog a, .login #nav a, .login h1 a {
    color: #fff !important;
}
.login #backtoblog a:hover, .login #nav a:hover, .login h1 a:hover {
    text-decoration: underline;
}
</style>
<?php
} add_action( 'login_enqueue_scripts', 'my_login_logo' );


//Menu support

add_theme_support('menus');
register_nav_menus(
  array(
    'primary_nav' => 'Primary Nav',
    'footer_nav' => 'Footer Nav',

  )
);

function theme_support_features() {
		add_theme_support( 'automatic-feed-links' ); //adds rss feed links to the head
		add_theme_support( 'structured-post-formats', array( 'link', 'video' ) );
		add_theme_support( 'post-formats', array( 'aside', 'audio', 'chat', 'gallery', 'image', 'quote', 'status' ) );
		add_theme_support( 'post-thumbnails' ); //thumbnail/featured image support
}
add_action( 'after_setup_theme', 'theme_support_features' );

// Adding Options Pages

if( function_exists('acf_add_options_page') ) {

    acf_add_options_page(array(
        'page_title'    => 'Header',
        'menu_title'    => 'Header Options',
        'menu_slug'     => 'header-options',
        'capability'    => 'edit_posts',
        'redirect'      => false
    ));

     acf_add_options_page(array(
        'page_title'    => 'Footer',
        'menu_title'    => 'Footer Options',
        'menu_slug'     => 'footer-options',
        'capability'    => 'edit_posts',
        'redirect'      => false
    ));

}

//hide gravity label

add_filter( 'gform_enable_field_label_visibility_settings', '__return_true' );

/**
 * If more than one page exists, return TRUE.
 */
function is_paginated() {
    global $wp_query;
    if ( $wp_query->max_num_pages > 1 ) {
        return true;
    } else {
        return false;
    }
}

/**
 * Rocket Effect additions
 */

function rocketeffect_theme_styles()
{
    wp_enqueue_style('rocketeffect', get_template_directory_uri() . '/public/css/rocketeffect.css', ['bootstrap'], '2.0.9.5');
}
add_action('wp_enqueue_scripts', 'rocketeffect_theme_styles');
