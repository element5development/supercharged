<?php
/*
Template Name: Home
*/
?>
<?php get_header(); ?>

<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

<div class="hero">                                            
	<div class="hero_slider">
	           <?php if( have_rows('hero_slider') ): ?>
                      <?php while ( have_rows('hero_slider') ) : the_row();  ?>
                      	<?php $image = wp_get_attachment_image_src(get_sub_field('image'), 'large');?>

                        <div class="hero_slide" style="background-image:url(<?php  echo $image[0]; ?>)">
                            <div class="hero_overlay">
                                <div class="hero_content">
                                    <div class="container">
                                        <h1><?php the_sub_field('content'); ?></h1>
                                    </div>
                                </div>
                            </div>
                        </div>  	

					  <?php  endwhile; ?>  
                       <?php endif; ?>


	</div>
	<div class="hero_bottom_overlay">
		<div class="hero_scroll">
			<a href="#main"><img src="<?php echo site_url(); ?>/wp-content/themes/supercharged/public/img/mouse-scroll.png"></a>
		</div>
	</div>
</div>
<?php if( have_rows('activities_sec') ): ?>
        <?php while ( have_rows('activities_sec') ) : the_row();  ?>
  
        <div class="se_main_content" id="main">
            <div class="container">
                <h2><?php the_sub_field('title'); ?></h2>
                <p><?php the_sub_field('content'); ?></p>
                <div class="se_attractions">
                    <div class="row">

                    <?php if( have_rows('activities_h') ): ?>
                              <?php while ( have_rows('activities_h') ) : the_row();  ?>
                                <?php $icon = wp_get_attachment_image_src(get_sub_field('icon'), 'large');?>

                                <div class="col-lg-3 col-md-6">
                                    <div class="se_attraction">
                                        <a href="<?php the_sub_field('link'); ?>">
                                            <div class="se_attraction_icon">
                                                <img src="<?php  echo $icon[0]; ?>">
                                            </div>
                                            <h3><?php the_sub_field('title'); ?></h3>
                                        </a>
                                    </div>
                                </div>		

                              <?php  endwhile; ?>  
                               <?php endif; ?>  


                    </div>
                </div>
                <?php if(get_sub_field('button_link')){ ?>
                    <div class="se_content_button">
                        <a href="<?php the_sub_field('button_link'); ?>" class="site_button site_button_inverse"><?php the_sub_field('button_text'); ?></a>
                    </div>
                <?php } ?>
            </div>
        </div>

    
    <?php  endwhile; ?>  
<?php endif; ?>


             <?php if( have_rows('view_menu_h') ): ?>
                  <?php while ( have_rows('view_menu_h') ) : the_row();  ?>


                        <div class="se_main_content_blue">


                            <div class="se_inner_content">
                                <?php if( have_rows('images') ): ?>
                                    <div class="row">
                                      <?php while ( have_rows('images') ) : the_row();  ?>
                                        <?php $image = wp_get_attachment_image_src(get_sub_field('image'), 'large');?>       
                                            <div class="col-sm-2 pub_grub_images" >
                                                <div class="pub_grub_image" style="background-image: linear-gradient(rgba(0, 0, 0, 0.25), rgba(0, 0, 0, 0.25)), url(<?php  echo $image[0]; ?>);"></div>
                                            </div>
                                        <?php  endwhile; ?>  
                                    </div>
                                <?php endif; ?>
                                <div class="pub_grub_wrap">
                                    <div class="container">
                                        <h2><?php the_sub_field('title'); ?></h2>
                                        <div class="se_content_button">
                                            <a href="<?php the_sub_field('button_link'); ?>" class="site_button">
                                                <?php the_sub_field('button_text'); ?>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>                        
                        </div>

                <?php  endwhile; ?>  
            <?php endif; ?>



                <?php if( have_rows('events_section') ): ?>
                    <?php while ( have_rows('events_section') ) : the_row();  ?>
                        <?php $background = wp_get_attachment_image_src(get_sub_field('background_image'), 'large');?>               
                        <div class="se_main_content se_main_content_reverse">
                            <div class="se_inner_content" style="background-image:url(<?php  echo $background[0]; ?>);">
                                <div class="container">
                                    <h2><?php the_sub_field('title'); ?></h2>
                                    <p><?php the_sub_field('content'); ?></p>
                                    <div class="se_content_button">
                                        <a href="<?php the_sub_field('button_link'); ?>" class="site_button site_button_inverse"><?php the_sub_field('button_text'); ?></a>
                                    </div>
                                </div>
                            </div>
                        </div>

                  <?php  endwhile; ?>  
            <?php endif; ?>



          <?php if( have_rows('info_section') ): ?>
                 <?php while ( have_rows('info_section') ) : the_row();  ?>
                    <?php $background = wp_get_attachment_image_src(get_sub_field('background_image'), 'large');?>      

                    <div class="se_main_content_background" style="background-image:url(<?php  echo $background[0]; ?>);">
                        <div class="container">
                            <h2><?php the_sub_field('info_title'); ?></h2>
                            <div class="se_attractions se_info">
                                <div class="row">

                                      <?php if( have_rows('info') ): ?>
                                          <?php while ( have_rows('info') ) : the_row();  ?>
                                            <?php 
                                            $icon = wp_get_attachment_image_src(get_sub_field('icon'), 'large');
                                            $page_link = get_sub_field('link');     
                                            $anchor = get_sub_field('anchor');     
                                            ?> 

                                            <div class="col-md-4">
                                                <div class="se_attraction">
                                                    <a href="<?php echo $page_link; ?><?php echo $anchor; ?>">
                                                        <div class="se_attraction_icon">
                                                            <img src="<?php  echo $icon[0]; ?>">
                                                        </div>
                                                        <h3><?php the_sub_field('title'); ?></h3>
                                                    </a>
                                                </div>
                                            </div>

                                       <?php  endwhile; ?>  
                                    <?php endif; ?>

                                </div>
                            </div>
                            <div class="se_content_button">
                                <a href="<?php the_sub_field('button_link'); ?>" class="site_button"><?php the_sub_field('button_text'); ?></a>
                            </div>
                        </div>
                    </div>
        <?php  endwhile; ?>  
    <?php endif; ?>


<?php endwhile; endif; ?>

<?php get_footer(); ?>
